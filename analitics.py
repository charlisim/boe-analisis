# -*- coding: utf-8 -*-
import lxml.html
import re
from lxml.html.clean import clean_html
from BeautifulSoup import BeautifulSoup


class Analisis:
	def __init__(self, toAnalize):
		self.analize = toAnalize
		self.analisisSoup = BeautifulSoup(self.analize)
		self.dataDict = {}
	def getData(self):
		allLis = self.analisisSoup.findAll('li')
		for li in allLis:		
			if re.findall('Rango:.*', str(li.contents[0])):
				self.dataDict['rango_ley'] = re.findall('Rango:.*', str(li.contents[0]))[0]
			if re.findall('Fecha de disposición:.*', str(li.contents[0])):
				self.dataDict['disposicion'] = re.findall('Fecha de disposición:.*', str(li.contents[0]))[0]
			if re.findall('Fecha de publicación:.*', str(li.contents[0])):
				self.dataDict['publicacion'] = re.findall('Fecha de publicación:.*', str(li.contents[0]))[0]
			if re.findall('Entrada en vigor.*', str(li.contents[0])):
				self.dataDict['vigor'] = re.findall('Entrada en vigor.*', str(li.contents[0]))[0]
		return self.dataDict

	def getMaterias(self):
		h5all = self.analisisSoup.findAll('h5')
		if h5all:
			for h5 in h5all:
				if h5.string == 'Materias':
					h5['id'] = 'materiash5'
					eachMaterias = []			
					materias = h5.findNextSibling('ul')
					for li in materias.findAll('li'):
				
						if li.string != '':					
							eachMaterias.append(li.string)


			
			return h5, materias, eachMaterias

