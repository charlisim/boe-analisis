# -*- coding: utf-8 -*-
import os
import sys, urlparse, urllib2, shutil, glob, robotparser
import query
import re
import sqlite3
from string import Template
import lxml.html
from lxml.html.clean import clean_html
from BeautifulSoup import BeautifulSoup
from database import Database
from analitics import Analisis

bd = sqlite3.connect('pycrawler.db')
database = Database()

def getPage(url):
	req = urllib2.Request(url)
	try:
		response = urllib2.urlopen(req)
		return response.read()
	except URLError, e:
		print e.reason
	


def saveInHTML(text, counter):
	filename = 'boe/%s.html'%(counter)
	f = open(filename, 'w+')
	f.seek(0)
	f.write(text)
	f.close()

def readTemplate(archivo):
	filename = open(archivo, 'r')
	content = filename.read()
	filename.close()
	return content

def extractID(element):
	match = re.split('\W+', element)
	format = '{0}-{1}-{2}-{3}'
	# print match
	if len(match) > 11:
		pass	
	else:
		newid = format.format(match[7], match[8], match[9],match[10])
		return newid, element


def processAnalisis(toAnalize):	
	analisis = Analisis(toAnalize)
	data = analisis.getData()
	materias = analisis.getMaterias()
			


	return data, materias
	

def template(element):
	
	elements = dict(id_boe = element[0],
					boe = element[1],
					titulo_boe = element[2],
					rango_ley = element[3]['rango_ley'],
					disposicion = element[3]['disposicion'],
					publicacion = element[3]['publicacion'],
					vigor = element[3]['vigor'],
					materias_name = element[4][0],
					materias = element[4][1]
					)

	html = readTemplate('boe/html_boe.html')
	html = Template(html).safe_substitute(elements)
	return html


def toDB(element):
	
	elementsToDB = dict(id_boe = str(element[0]),
						titulo_boe = element[2].string,
						URL = element[5]	

					
					)
	if element[3]:
			
			if 'rango_ley' in element[3]:
				elementsToDB['rango_ley'] = element[3]['rango_ley']
			if 'disposicion' in element[3]:
				elementsToDB['disposicion'] = element[3]['disposicion']
			if 'publicacion' in element[3]:
				elementsToDB['publicacion'] = element[3]['publicacion']
			if 'vigor' in element[3]:
				elementsToDB['vigor'] = element[3]['vigor']
	if element[4]:		
		materias = element[4][2]
		elementsToDB['materias'] = materias

	database.dbinsertData(elementsToDB)

def processURL(element):
	if len(extractID(element)) >= 2:
		newid, URL = extractID(element)
		print database.laws.find({'id_boe' : newid}).count()
		if database.laws.find({'id_boe' : newid}).count() > 0:
			pass
		else:
			print newid
			pagehtml = getPage(element)
			soup = BeautifulSoup(pagehtml)
			boe_body = soup.find(id='textoxslt')
			title = soup.find('h3', {'class':'documento-tit'})	
			analisis = soup.find('div', {'class': 'analisisDoc'})
			rAnalisis = processAnalisis(str(analisis))
			toTemplate = []
			toTemplate.append(newid)		
			toTemplate.append(boe_body)
			toTemplate.append(title)
			toTemplate.append(rAnalisis[0])
			toTemplate.append(rAnalisis[1])
			toTemplate.append(URL)
			# html = template(toTemplate)
			toDB(toTemplate)
			# saveInHTML(html,newid)




selectedobjects = []
for row in bd.execute('SELECT address FROM crawl'):	
	for element in row:		
		if element.find('txt.php?') != -1:			
			selectedobjects.append(element)


for el in selectedobjects:	
	processURL(el)
	

			
		


